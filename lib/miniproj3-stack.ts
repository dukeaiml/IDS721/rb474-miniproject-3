import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class Miniproj3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // create s3 bucket with versioning and encryption
    const bucket = new cdk.aws_s3.Bucket(this, 'miniproj3', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED,
    }
    )
    // example resource
    // const queue = new sqs.Queue(this, 'Miniproj3Queue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
