# rb474-miniproject-3

The aim of the miniproject is to create S3 bucket with encryption and versioning using AWS CDK with assistance of CodeWhisperer (the tool is like Github Copilot, that is, it is an AI assistant which suggests how to continue a piece of code).

## Creation process
1. Open **CodeCatalyst**.
2. Create a user with no rights in AWS Management Console.
3. Get the access credentials.
4. Use them in `aws configure`.
5. Initiate the project with `cdk init app --language typescript` in an empty directory.
6. With the help of **CodeWhisperer**, change **lib/[PROJECTNAME]-stack.ts**. I prompted `" // create s3 bucket with versioning and encryption"` (and waited until I got suggestions, this is the only part when I used CodeWhisperer in the miniproject). For **/bin/[PROJECTNAME].ts**, it is enough to uncomment the default settings.
7. `npm run build`
8. Try `cdk synth`. It will fail because your user does not have the right permissions. Add them by trying to understand the error messages (my user had **AdministratorAccess** and **CloudFormation**).
9. Run `cdk synth`, then `cdk deploy`. Viola!

## Screenshots of an example of a result
### Versioning
![](images/versioning.png)

### Encryption
![](images/encryption.png)